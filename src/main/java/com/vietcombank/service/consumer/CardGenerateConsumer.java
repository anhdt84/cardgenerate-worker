package com.vietcombank.service.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vietcombank.service.model.CardGenerateRequestModel;
import com.vietcombank.service.model.NotifyRequest;
import com.vietcombank.service.model.NotifyResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigInteger;
import java.util.Random;
import java.util.UUID;

@Component
@Slf4j
public class CardGenerateConsumer {
    @Autowired
    RestTemplate restTemplate;

    @Value("${notifyUrl}")
    String notifyUrl;

    @RabbitListener(queues = "${queue_name}", concurrency = "${concurrency}")
    public void recievedMessage(CardGenerateRequestModel cardGenerateRequestModel) {
        log.info("Recieved Message From RabbitMQ: " + cardGenerateRequestModel);
        try {
            //UUID uuid = UUID.randomUUID();
            cardGenerateRequestModel.setCardNumber(getRandomeCard(13));
            //1. Cap nhat ket qua vao Cardmanagement Service
//            NotifyRequest notifyRequest = new NotifyRequest();
//            notifyRequest.setContent("The cua ban da duoc san sang:" + cardGenerateRequestModel.getCardNumber());
//            notifyRequest.setPhonenumber(cardGenerateRequestModel.getPhoneNumber());
//            notifyRequest.setRefnum("123456");
//            try {
//                ResponseEntity<NotifyResponse> sendSMSResponse = restTemplate.postForEntity(notifyUrl, notifyRequest, NotifyResponse.class);
//                log.debug("Finished send to dest:Result:" + sendSMSResponse.getBody().toString());
//            } catch (Exception ex) {
//                log.error(ex.getMessage(),ex);
//            }

            //2. Ban Notify
            NotifyRequest notifyRequest = new NotifyRequest();
            notifyRequest.setContent("The cua ban da duoc san sang:" + cardGenerateRequestModel.getCardNumber());
            notifyRequest.setPhonenumber(cardGenerateRequestModel.getPhoneNumber());
            notifyRequest.setRefnum("123456");
            try {
                ResponseEntity<NotifyResponse> sendSMSResponse = restTemplate.postForEntity(notifyUrl, notifyRequest, NotifyResponse.class);
                log.debug("Finished send to dest:Result:" + sendSMSResponse.getBody().toString());
            } catch (Exception ex) {
                log.error(ex.getMessage(),ex);
            }

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    private String getRandomeCard(int length) {
        try {
            Random rd = new Random();
            StringBuilder result = new StringBuilder();
            result.append("970436");
            for (int index = 0; index < length; index++) {
                result.append(String.valueOf(rd.nextInt(10)));
            }
            log.info("getRandomeCard result = " + result.toString());
            return result.toString();

        } catch (Exception ex) {
            return "";
        }
    }
}
