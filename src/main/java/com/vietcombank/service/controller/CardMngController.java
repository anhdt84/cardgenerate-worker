package com.vietcombank.service.controller;

import com.vietcombank.service.entity.DeviceKeyEntity;
import com.vietcombank.service.model.CardGenerateRequestModel;
import com.vietcombank.service.service.DeviceKeyService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@Slf4j
@RestController
@RequestMapping("/cardmngt")
public class CardMngController {
    @Autowired
    private DeviceKeyService deviceService;

    @Autowired
    private AmqpTemplate rabbitTemplate;
    @Value("${queue_name}")
    String queuesName;

    @PostMapping("/v1/genCard")
    private ResponseEntity<?> genCard(@RequestBody CardGenerateRequestModel entity) {
        log.info(entity.toString());
        try {
            rabbitTemplate.convertAndSend(queuesName, entity);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
