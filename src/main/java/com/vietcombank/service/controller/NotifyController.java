package com.vietcombank.service.controller;

import com.vietcombank.service.entity.DeviceKeyEntity;
import com.vietcombank.service.model.CardGenerateRequestModel;
import com.vietcombank.service.service.DeviceKeyService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Value;

import java.util.NoSuchElementException;

@Slf4j
@RestController
@RequestMapping("/notifyapi")
public class NotifyController {
    @Autowired
    private DeviceKeyService deviceService;

    @Autowired
    private AmqpTemplate rabbitTemplate;
    @Value("${queue_name}")
    String queuesName;

    @GetMapping(value = "/v1/getAllDeviceKey", produces = "application/json")
    @Operation(summary = "Retrieve All devicekey")
    public ResponseEntity<?> getAllDeviceKey() {
        log.info("retrieve all device keyss");
        return ResponseEntity.ok(deviceService.retrieveAll());
    }

    //creating post mapping that post the book detail in the database
    @PostMapping("/v1/saveDeviceKey")
    private ResponseEntity<?> saveDeviceKey(@RequestBody DeviceKeyEntity entity) {
        try {
            deviceService.save(entity);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/v1/genCard")
    private ResponseEntity<?> genCard(@RequestBody CardGenerateRequestModel entity) {
        log.info(entity.toString());
        try {
            rabbitTemplate.convertAndSend(queuesName, entity);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/v1/getById/{id}")
    public ResponseEntity<?> getDeviceKeyById(@PathVariable String id) {
        try {
            DeviceKeyEntity existProduct = deviceService.getById(id);
            return ResponseEntity.ok(existProduct);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/v1/updateDeviceKey")
    public ResponseEntity<?> update(@RequestBody DeviceKeyEntity deviceKeyEntity) {
        try {
            deviceService.save(deviceKeyEntity);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/v1/deleteDeviceKey/{id}")
    public ResponseEntity<?> deleteDeviceKey(@PathVariable String id) {
        try {
            deviceService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
