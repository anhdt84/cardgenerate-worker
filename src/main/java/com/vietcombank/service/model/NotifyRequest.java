package com.vietcombank.service.model;

import lombok.Data;

@Data
public class NotifyRequest {
    private String refnum ;
    private String phonenumber ;
    private String content ;
}
