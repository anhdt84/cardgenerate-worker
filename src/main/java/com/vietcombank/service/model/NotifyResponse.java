package com.vietcombank.service.model;

import lombok.Data;

@Data
public class NotifyResponse {
    private String respCode;
    private String respDesc;
}
