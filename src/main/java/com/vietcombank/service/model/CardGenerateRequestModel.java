package com.vietcombank.service.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id", scope = com.vietcombank.service.model.CardGenerateRequestModel.class)
public class CardGenerateRequestModel implements Serializable {
    String requestId;
    String cif;
    String account;
    String phoneNumber;
    int limit;
    String productCode;
    Date expireDate;
    String cardNumber;
}
